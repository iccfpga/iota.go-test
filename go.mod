module gitlab.com/iccfpga/iota.go-test

go 1.12

replace github.com/iotaledger/iota.go v1.0.0-beta => ../iota.go

require (
	github.com/iotaledger/iota.go v1.0.0-beta
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
)
