package main

import (
	"fmt"

	. "github.com/iotaledger/iota.go/api"
	"github.com/iotaledger/iota.go/bundle"
	. "github.com/iotaledger/iota.go/consts"
	"gitlab.com/iccfpga/iota.go-test/iccfpga"
)

// has to be kept secret ... the weakness of everything :scream:
var apiKey = [...]byte{0xb2, 0x33, 0x12, 0x56, 0x41, 0xf0, 0xcc, 0x60, 0x9c, 0x6f, 0x36, 0x30, 0xe7, 0xf3, 0xb2, 0xfd, 0xb7, 0x2b, 0xbd, 0x2e,
	0x94, 0x40, 0xa5, 0xb0, 0x0a, 0xb5, 0x83, 0x65, 0x5b, 0x01, 0xb1, 0x43, 0xdf, 0x31, 0x3e, 0x9a, 0xa0, 0x90, 0x72, 0x02, 0xdf, 0x5f, 0x16,
	0x40, 0x8e, 0x64, 0xf4, 0xd4}

var endpoint = "https://nodes.thetangle.org:443"

//var endpoint = "nodes.thetangle.org:443"

// difficulty of the proof of work required to attach a transaction on the tangle
const mwm = 14

// how many milestones back to start the random walk from
const depth = 3

// currently needed for validation
const dummySeed = "999999999999999999999999999999999999999999999999999999999999999999999999999999999"

var flags map[string]interface{}

var keyIndex uint32

func main() {
	err := iccfpga.Init(iccfpga.Flags{KeepSeedInRAM: true})
	must(err)

	err = iccfpga.GenerateRandomSeed(0)
	must(err)

	// create a new API instance
	api, err := ComposeAPI(HTTPClientSettings{
		URI: endpoint,
		// (!) if no PoWFunc is supplied, then the connected node is requested to do PoW for us
		// via the AttachToTangle() API call.
		LocalProofOfWorkFunc:       iccfpga.PoWFunc,
		LocalSignInputsFunc:        iccfpga.SignInputs,
		LocalGenerateAddressesFunc: iccfpga.GenerateAddresses,
	})
	must(err)
	for key := uint64(0); key < 1000; key++ {
		//key := uint64(2)

		addrArray, err := iccfpga.GenerateAddresses("", 0, 1, SecurityLevelMedium)
		addr := addrArray[0]
		fmt.Println(addr)

		// create a transfer to the given recipient address
		// optionally define a message and tag
		transfers := bundle.Transfers{
			{
				Address: addr,
				Value:   1000,
				Tag:     "ICCFPGA9TEST",
			},
		}

		// create inputs for the transfer
		inputs := []Input{
			{
				Address:  addr,
				Security: SecurityLevelMedium,
				KeyIndex: key,
				Balance:  1000,
			},
		}

		// create an address for the remainder.
		// in this case we will have 20 iotas as the remainder, since we spend 100 from our input
		// address and only send 80 to the recipient.
		//remainderAddress, err := address.GenerateAddress("", 1, SecurityLevelMedium)
		//must(err)

		// we don't need to set the security level or timestamp in the options because we supply
		// the input and remainder addresses.
		prepTransferOpts := PrepareTransfersOptions{Inputs: inputs}
		//	prepTransferOpts := PrepareTransfersOptions{Inputs: inputs, RemainderAddress: &remainderAddress}

		// prepare the transfer by creating a bundle with the given transfers and inputs.
		// the result are trytes ready for PoW.
		trytes, err := api.PrepareTransfers(dummySeed, transfers, prepTransferOpts)
		must(err)

		// you can decrease your chance of sending to a spent address by checking the address before
		// broadcasting your bundle.
		/*	spent, err := api.WereAddressesSpentFrom(transfers[0].Address)
			must(err)

			if spent[0] {
				fmt.Println("recipient address is spent from, aborting transfer")
				return
			}
		*/
		// at this point the bundle trytes are signed.
		// now we need to:
		// 1. select two tips
		// 2. do proof-of-work
		// 3. broadcast the bundle
		// 4. store the bundle
		// SendTrytes() conveniently does the steps above for us.
		bndl, err := api.SendTrytes(trytes, depth, mwm)
		must(err)

		fmt.Println("broadcasted bundle with tail tx hash: ", bundle.TailTransactionHash(bndl))
	}
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
