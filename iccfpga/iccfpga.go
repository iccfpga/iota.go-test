package iccfpga

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"

	"encoding/json"

	. "github.com/iotaledger/iota.go/api"
	. "github.com/iotaledger/iota.go/consts"
	"github.com/iotaledger/iota.go/kerl"
	"github.com/iotaledger/iota.go/trinary"
	. "github.com/iotaledger/iota.go/trinary"
	"github.com/tarm/serial"

	keccak "github.com/iotaledger/iota.go/kerl/sha3"
)

// has to be kept secret ... the weakness of everything :scream:
var apiKey = [...]byte{0xb2, 0x33, 0x12, 0x56, 0x41, 0xf0, 0xcc, 0x60, 0x9c, 0x6f, 0x36, 0x30, 0xe7, 0xf3, 0xb2, 0xfd, 0xb7, 0x2b, 0xbd, 0x2e,
	0x94, 0x40, 0xa5, 0xb0, 0x0a, 0xb5, 0x83, 0x65, 0x5b, 0x01, 0xb1, 0x43, 0xdf, 0x31, 0x3e, 0x9a, 0xa0, 0x90, 0x72, 0x02, 0xdf, 0x5f, 0x16,
	0x40, 0x8e, 0x64, 0xf4, 0xd4}

var flags map[string]interface{}

var comm = &serial.Config{Name: "/dev/ttyUSB1", Baud: 115200}
var port *serial.Port

// Flags holds all allowed flags
type Flags struct {
	KeepSeedInRAM bool
}

// Init initialize rs232 connection and set flags on fpga
func Init(f Flags) error {
	flags = make(map[string]interface{})
	flags["keepSeedInRAM"] = f.KeepSeedInRAM

	comm = &serial.Config{Name: "/dev/ttyUSB1", Baud: 115200}

	var err error
	port, err = serial.OpenPort(comm)
	if err != nil {
		return err
	}

	_, err = apiSetFlags(flags)
	if err != nil {
		return err
	}
	return nil
}

func GenerateAddresses(seed Trytes, index uint64, total uint64, secLvl SecurityLevel, addChecksum ...bool) ([]Hash, error) {
	addresses := make(Hashes, total)

	number := int(total)
	ofs := int(index)
	for number > 0 {
		chunk := min(number, 100)
		trytes, err := apiGenerateAddress(0, ofs, int(secLvl), chunk)
		if err != nil {
			return nil, err
		}
		for j := 0; j < chunk; j++ {
			addresses[j] = trytes[j]
		}
		ofs += chunk
		number -= chunk
	}
	return addresses, nil
}

func min(a int, b int) int {
	if a <= b {
		return a
	} else {
		return b
	}
}

func SignInputs(inputs []Input, bundleHash Trytes) ([]Trytes, error) {
	signedFrags := []Trytes{}
	for i := range inputs {
		input := &inputs[i]
		var sec SecurityLevel
		if input.Security == 0 {
			sec = SecurityLevelMedium
		} else {
			sec = input.Security
		}

		frags := make([]Trytes, input.Security)
		signedTrytes, err := apiSignTransaction(0, uint32(input.KeyIndex), bundleHash, uint32(sec))
		if err != nil {
			return nil, err
		}
		/*		ok, err := signing.ValidateSignatures(input.Address, signedTrytes, finalizedBundle[0].Bundle)
				if err != nil || !ok {
					return nil, err
				}
		*/
		for i := 0; i < int(input.Security); i++ {
			frags[i] = signedTrytes[i]
		}

		signedFrags = append(signedFrags, frags...)
	}
	return signedFrags, nil
}

// PoWFunc PowFunc-Wrapper for PoW
func PoWFunc(trytes Trytes, mwm int, parallelism ...int) (Trytes, error) {
	return apiDoPoW(trytes, mwm)
}

func apiCall(cmd map[string]interface{}) (map[string]interface{}, error) {
	//time.Sleep(500 * time.Millisecond)
	bytes, err := json.Marshal(cmd)
	bytes = append(bytes, '\n')

	written, err := port.Write(bytes)
	if err != nil {
		return nil, errors.New("error writing data")
	}

	if written != len(bytes) {
		return nil, errors.New("not all bytes written")
	}

	reader := bufio.NewReader(port)
	reply, err := reader.ReadBytes('\x0a') // read until \n
	if err != nil {
		return nil, err
	}

	var response map[string]interface{}
	err = json.Unmarshal([]byte(reply), &response)
	if err != nil {
		return nil, err
	}

	if int(response["code"].(float64)) != 200 { // something went wrong ...
		return nil, errors.New(response["error"].(string))
	}

	fmt.Printf("%dms\n", int(response["duration"].(float64)))

	return response, nil
}

// care for endianess!
func u32ToBytes(value uint32) []byte {
	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.LittleEndian, value)
	if err != nil {
		return nil
	}
	return buf.Bytes()
}

// SignTransaction signs a transaction
func apiSignTransaction(key uint32, addressIndex uint32, bundleHash Trytes, securityLevel uint32) ([]Trytes, error) {
	// create api hash
	sha := keccak.New384()
	sha.Write(u32ToBytes(key))
	sha.Write(u32ToBytes(addressIndex))
	sha.Write([]byte(string(bundleHash)))
	sha.Write(apiKey[:])
	trits, err := kerl.KerlBytesToTrits(sha.Sum(nil))
	if err != nil {
		return nil, err
	}

	apiHash, err := trinary.TritsToTrytes(trits)
	if err != nil {
		return nil, err
	}

	cmd := make(map[string]interface{})
	cmd["command"] = "signTransaction"
	cmd["key"] = key
	cmd["addressIndex"] = addressIndex
	cmd["bundleHash"] = string(bundleHash)
	cmd["securityLevel"] = securityLevel
	cmd["auth"] = string(apiHash)

	response, err := apiCall(cmd)
	if err != nil {
		return nil, err
	}

	responseTrytes := response["trytes"].([]interface{})
	var list []Trytes
	for _, trytes := range responseTrytes {
		slice := Trytes(trytes.(string))
		list = append(list, slice[0:2187])
	}
	return list, nil
}

func GenerateRandomSeed(key int) error {
	return apiGenerateRandomSeed(key)
}

// GenerateRandomSeed on FPGA
func apiGenerateRandomSeed(key int) error {
	cmd := make(map[string]interface{})
	cmd["command"] = "generateRandomSeed"
	cmd["key"] = key
	_, err := apiCall(cmd)
	if err != nil {
		return err
	}

	return nil
}

// GenerateAddress generate Address on FPGA
func apiGenerateAddress(key int, index int, security int, number int) ([]Trytes, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "generateAddress"
	cmd["firstIndex"] = index
	cmd["number"] = number
	cmd["security"] = security
	cmd["key"] = key
	response, err := apiCall(cmd)
	if err != nil {
		return nil, err
	}

	responseTrytes := response["trytes"].([]interface{})
	var list []Trytes
	for _, trytes := range responseTrytes {
		list = append(list, Trytes(trytes.(string)))
	}
	return list, nil
}

// DoPoW do PoW
func apiDoPoW(trytes Trytes, mwm int) (Trytes, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "doPow"
	cmd["trytes"] = [1]string{trytes}
	cmd["minWeightMagnitude"] = mwm
	response, err := apiCall(cmd)
	if err != nil {
		return "", err
	}

	responseTrytes := response["trytes"].([]interface{})
	return Trytes(responseTrytes[0].(string)), nil
}

func apiSetFlags(flags map[string]interface{}) (map[string]interface{}, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "setFlags"
	cmd["flags"] = flags
	return apiCall(cmd)
}
